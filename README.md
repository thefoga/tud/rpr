# attila's report
**A**t**T**en**T**ion **I**n ce**L**l segment**A**tion

## LaTeX reports

- download **daily builds** from [here](https://gitlab.com/thefoga/tud/rpr/-/blob/main/main.pdf)
- [nightly builds](https://gitlab.com/thefoga/tud/rpr/-/jobs/artifacts/main/raw/main.pdf?job=build) (docker based)

# Code

- in [GitHub](https://github.com/sirfoga/attila)
