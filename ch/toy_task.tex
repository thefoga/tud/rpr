\input{./data/toy_task}

\chapter{Dense image segmentation task}
\label{cap:toy_task}

This chapter describes what has been done with respect to the segmentation task of cell nucleus images: a comparative study of the hyperparameters that define an abstract U-Net architecture. The concrete models are compared with respect to classic metrics (Mean IoU and DSC).

\section{Objective}

The aim is to

\begin{enumerate}
    \item compare different methods of (supervised) segmentation
    \item develop a baseline
    \item match SOTA
\end{enumerate}

\section{Data}
\label{sec:toy_task-data-data}

The aim is to segment images in the cell nucleus dataset (see \ref{subsec:toy_task-data-intro}).

\section{Underlying architecture}
\label{sec:toy_task-architecture}

The architecture builds on top of well-known and existing architectures (see \ref{sec:intro-nn}).

\subsection{U-Net}
\label{subsec:toy_task-architecture-unet}

\subsubsection{History}
\label{subsubsec:toy_task-architecture-unet-history}

\cite{paper:unet} was a breakthrough in the field of image segmentation. They reached a new \sota.

\subsubsection{Architecture}
\label{subsubsec:toy_task-architecture-unet-architecture}

The NN consists of 3 \textit{paths}/phases:
\begin{enumerate}
    \item first the input gets convoluted and max-pooled in the so called \textit{contracting path}
    \item in the lowest path (\ie{} highest abstraction) only 1 convolution is performed
    \item the expanding path upsamples and convolutes the feature maps
\end{enumerate}
Finally the last convolution maps each pixel to the most probable class.

\subsubsection{Skip connection}
\label{subsubsec:toy_task-architecture-unet-skipconnection}

In each layer of the expanding path the output of the previous layer gets concatenated with the correspondingly cropped feature map from the contracting path.\\
Not employing the skip connections in the expanding path reduces the UNet to an autoencoder: the output is expected to be a little bit blurrier since there is less information to reconstruct the output.

\subsection{USE-Net}
\label{subsec:toy_task-architecture-usenet}

\subsubsection{History}
\label{subsubsec:toy_task-architecture-usenet-history}

\cite{paper:SE} suggests a simple and easy way to learn how to activate each channel of a feature map. The proposed block can be plug-in in most of existing architectures, thus enabling performance improvement at virtually no cost, like in \cite{paper:usenet}.

\subsubsection{SE block}
\label{subsubsec:toy_task-architecture-usenet-seblock}

The block consists of 2 phases:
\begin{enumerate}
    \item \textit{squeezing} the feature map into a vector by global average pooling
    \item \textit{exciting} the vector by passing it through a gating mechanism formed by 2 fully connected layers
\end{enumerate}

\section{Chosen architecture}
\label{sec:toy_task-model}

The chosen model is not a U-Net nor a USE-Net: it's a \textit{Frankensteinian} NN that is developed from both of them. The architecture is defined by some hyperparameters:

\begin{enumerate}
  \item usage of skip connections \see[subsubsec:toy_task-architecture-unet-skipconnection]: whether to use or not skip connections in the expanding path
  \item usage of SE block \see[subsubsec:toy_task-architecture-usenet-seblock]: whether to use or not a SE block after each convolutional layer
  \item padding: as defined in \href{https://keras.io/api/layers/convolution_layers/convolution2d/}{Keras documentation}
\end{enumerate}

\section{Metrics}
\label{sec:toy_task-metrics}

\subsection{Preliminaries}
\label{subsec:toy_task-metrics-pre}

Let, without loss of generality, a binary image consisting of
\begin{enumerate}
    \item predicted mask, \ie{} a set of pixels labeled as \texttt{foreground} and the rest as \texttt{background}
    \item original mask
\end{enumerate}
then, given
\begin{enumerate}
    \item TP are the pixels correctly labeled as \texttt{foreground}
    \item TN are the pixels correctly labeled as \texttt{background}
    \item FP are the pixels wrongly labeled as \texttt{foreground}
    \item FN are the pixels wrongly labeled as \texttt{background}
\end{enumerate}
we define
\begin{enumerate}
    \item \textit{intersection} = TP
    \item \textit{union} = TP + FP + FN
\end{enumerate}
Note that we can calculate
\begin{equation}
    \text{intersection} = \sum_{y \in \text{pixels}} y_{\text{ground truth}} \cdot y_{\text{pred}}
\label{metrics_i}
\end{equation}
and
\begin{equation}
    \text{union} = \sum_{y \in \text{pixels}} y_{\text{ground truth}} + y_{\text{pred}} - \text{intersection}
\label{metrics_u}
\end{equation}

\subsection{Chosen metrics}
\label{subsec:toy_task-metrics-chosen}

Chosen metrics are:
\begin{enumerate}
  \item Intersection over Union, \aka{} IoU: $\frac{\text{intersection}}{\text{union}} = \frac{TP}{TP + TN + FN}$ which is the basis for mean IoU, \ie{} the mean IoU over all classes.
  \item Sørensen–Dice coefficient, \aka{} DSC: $\frac {2 \cdot TP}{2 \cdot TP + TN + FN}$. Usually a \textit{smoothing coefficient} is added to both numerator and denominator: $\frac {2 \cdot TP + eps}{2 \cdot TP + TN + FN + eps}$
\end{enumerate}

In this case the metrics were evaluated only over \texttt{foreground} and \texttt{borders}: the \texttt{background} has not been considered. Both metrics are evaluated on the center-cropped (based on the output of \texttt{padding = valid} configuration) predictions, to allow direct comparison of padding types.

\section{Loss function}
\label{sec:igpt_task_loss}

The usual binary cross-entropy classification loss $\mathcal{L}_{BCE}$ (with sigmoid activation in the last layer) is considered for the prediction of 2 classes (\texttt{foreground} and \texttt{background}).\\

In the case of the additional \texttt{borders} class, the categorical cross-entropy classification loss $\mathcal{L}_{CCE}$ (with softmax activation in the last layer) is employed.\\

Experiments were made on a weighted version of $\mathcal{L}_{CCE}$ to give more weight to the \texttt{borders} class but results were not too drammatically different.

\section{Implementation}
\label{sec:unet-implementation}

\href{https://keras.io/guides/functional_api/}{Functional API} from \href{https://keras.io/}{Keras} is used to implement the NN in \href{https://www.python.org/}{Python}.\\

No data/model parallelism is being implemented. Data is fed to the NN by a \href{https://keras.io/api/preprocessing/image/#imagedatagenerator-class}{generator} in order to have comparable results between data-augmented experiments and not-data-augmented.

\section{Experiments}
\label{sec:toy_task-small-experiments}

\subsection{Setup}
\label{sec:toy_task-small-experiments_setup}

Call \textit{experiment run} (or simply \textit{run}) the process of
\begin{enumerate}
    \item choosing a hyper-parameter combination
    \item build the model with such combination
    \item run the model on a given training, validation, test split data
\end{enumerate}

To have meaningful results the test data is chosen at first and remains the same for all runs. The training and validation set does change between runs but not between experiments in the same run.\\

In case of multiple runs the final evaluation statistics is the mean on all the runs.

\subsection{Data augmentation}
\label{sec:toy_task-small-experiments_augm}

In order to see if data augmentation plays a role (it does) a very low amount of training data ($3.5 \%$, about $3$ images) was reserved for training on the vanilla U-Net configuration (using skip connections, not using SE blocks).\\

Results are shown in \fg[small_experiments_augs]{} alongside \fg[small_experiments_no_aug]{} and \fg[small_experiments_aug]{} depicting the effect of training/test splits.

\subsection{Different optimizers}
\label{sec:toy_task-small-experiments_optim}

In order to explore the effect of different loss optimizers, \href{https://keras.io/api/optimizers/adam/}{Adam} with different learning rates ($\{ 1e-2, \ldots, 1e-5 \}$) and \href{https://keras.io/api/optimizers/sgd/}{SGD} with initial learning rate $= 1e-2$ and high momentum ($0.99$, just like in \cite{paper:unet}) were compared.\\

Skip connections were used. Results are shown in \fg[small_experiments_lrs]{} along with some sample training histories in \fg[small_experiments_optimizers]{}.

\subsection{Hyper-parameters}
\label{sec:toy_task-experiments}

In this case \nruns{} runs were completed:
\begin{enumerate}
    \item \tab[exp-base-config] shows the experiment base configuration
    \item \tab[usenet-results] shows the results
    \item \fg[usenet-history] shows last \lastepochs{} epochs of training of last run
    \item \fg[usenet-img-results] shows some sample results
\end{enumerate}

\begin{table*}[htp]
\centering
\rowcolors{2}{black!10}{black!40}
\begin{tabular}{*7l} \toprule
skip connections? & SE blocks? & padding & \emph{Mean IoU} & \emph{DSC} \\ \midrule

\cmark{} & \xmark{} & \texttt{same} & \textbf{0.984} $\pm$ 0.066 & \textbf{0.991} $\pm$ 0.043 \\
\xmark{} & \xmark{} & \texttt{same} & 0.971 $\pm$ 0.087 & 0.984 $\pm$ 0.061 \\
\cmark{} & \xmark{} & \texttt{valid} & 0.979 $\pm$ 0.074 & 0.988 $\pm$ 0.051 \\
\xmark{} & \xmark{} & \texttt{valid} & 0.970 $\pm$ 0.093 & 0.982 $\pm$ 0.066 \\
\cmark{} & \cmark{} & \texttt{same} & 0.980 $\pm$ 0.072 & 0.989 $\pm$ 0.050 \\
\xmark{} & \cmark{} & \texttt{same} & 0.965 $\pm$ 0.098 & 0.980 $\pm$ 0.068 \\
\cmark{} & \cmark{} & \texttt{valid} & 0.978 $\pm$ 0.077 & 0.987 $\pm$ 0.053 \\
\xmark{} & \cmark{} & \texttt{valid} & 0.967 $\pm$ 0.092 & 0.981 $\pm$ 0.063 \\

\bottomrule
\hline
\end{tabular}
\caption{Segmentation experiments results (mean of \nruns{} runs) on cell nucleus data}
\label{tab:usenet-results}
\end{table*}

\begin{figure*}[htp]
    \centering
    \showSubFig{60mm}{\figDir/no-aug/with_same/input_\noAugPredToShow.png}{input image}
    
    \centering
    \showPredOfModel{with_same}{no-aug}{\noAugPredToShow} \showPredOfModel{without_same}{no-aug}{\noAugPredToShow} \showPredOfModel{with_valid}{no-aug}{\noAugPredToShow} \\
    \showPredOfModel{without_valid}{no-aug}{\noAugPredToShow} \showPredOfModel{with_same_se}{no-aug}{\noAugPredToShow} \showPredOfModel{without_same_se}{no-aug}{\noAugPredToShow} \\
    \showPredOfModel{with_valid_se}{no-aug}{\noAugPredToShow} \showPredOfModel{without_valid_se}{no-aug}{\noAugPredToShow} \\
    
    \caption{Sample segmentations (using different models) of cell nucleus dataset}
    \label{fig:usenet-img-results}
\end{figure*}

\subsubsection{Data augmentation}
\label{subsec:toy_task-experiments-data-augm}

A second set of experiment runs has been carried out after augmenting data \see[subsec:toy_task-data-augmentation]. As previously, 5 runs were completed on the same configurations (see \tab[exp-base-config]):
\begin{enumerate}
    \item \tab[usenet-results-augm] shows the results
    \item \fg[usenet-history-augm] shows last \lastepochs{} epochs of training of last run
    \item \fg[usenet-img-results-augm] shows some sample predictions
\end{enumerate}

\begin{table*}[htp]
\centering
\rowcolors{2}{black!10}{black!40}
\begin{tabular}{*5l} \toprule
skip connections? & SE blocks? & padding & \emph{Mean IoU} & \emph{DSC} \\ \midrule

\cmark{} & \xmark{} & \texttt{same} & \textbf{0.986} $\pm$ 0.059 & \textbf{0.993} $\pm$ 0.038 \\
\cmark{} & \cmark{} & \texttt{same} & 0.981 $\pm$ 0.074 & 0.989 $\pm$ 0.051 \\
\cmark{} & \xmark{} & \texttt{valid} & 0.980 $\pm$ 0.074 & 0.989 $\pm$ 0.051 \\

\bottomrule
\hline
\end{tabular}
\caption{Segmentation experiments results (mean of \nruns{} runs) on augmented cell nucleus data (TOP 3 models)}
\label{tab:usenet-results-augm}
\end{table*}

\begin{figure*}[htp]
    \centering
    \showSubFig{60mm}{\figDir/aug/with_same/input_\augPredToShow.png}{input image}
    
    \centering
    \showPredOfModel{with_same}{aug}{\augPredToShow} \showPredOfModel{with_valid}{aug}{\augPredToShow} \showPredOfModel{with_same_se}{aug}{\augPredToShow} \\
    
    \caption{Sample segmentations (using different models) of cell nucleus augmented data}
    \label{fig:usenet-img-results-augm}
\end{figure*}
