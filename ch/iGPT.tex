\input{./data/iGPT}

\chapter{Generative Image Models with Transformers}
\label{cap:igpt}

This chapter analyzes the usage of Transformer-based architectures in the unsupervised segmentation problem:

\begin{enumerate}
    \item unsupervised training is done to extract meaningful features that represents an image
    \item sampling \textit{new} (out of the training set distribution) images
    \item embedding layer replacement
\end{enumerate}

\section{Objective}

The aim is to

\begin{enumerate}
    \item learn (unsupervisedly) a feature representation of data, \ie{} the NN has to know what are the intrinsic features of a sample
    \item match SOTA
    \item finetuning learned model in segmentation task
    \item compare with supervised learning
\end{enumerate}

\section{Data}
\label{sec:igpt_task_data}

Images used are those belonging in the EM flybrain dataset (see \ref{subsec:toy_task-data-em}).

\section{Underlying architecture}
\label{sec:igpt_task_arch}

Historically language and vision are the most (over)-valued fields of machine learning. Since the first NNs architectures, these two fields were completely separated: one technology advancement in one field could not immediately transferred to the other.\\

This has changed recently and the whole literature is moving towards a single architecture for (pretty much) all problems: the transformer.\\

The innovation behind it is \textit{simple} but effective, and mirrors how humans perceive the meaning of their physical inputs. The idea has its costs (high intensity computations are required) and a \textit{pletora} of research is being done to solve these problems. Nonetheless the idea has already produced above-expectation results and promises to \href{https://www.nature.com/articles/d41586-020-03348-4}{change everything}.

\subsection{Transformer}
\label{subsec:igpt_task-architecture_Transformer}

Transformers were first conceived by NLP researchers in the context of learning word (token) embeddings. \cite{paper:att_need} proposes to forget about convolutions (CNNs) and recurrences (RNNs, LSTMs) and let the NN learn relying solely on attention mechanisms.\\

The architecture resembles what \cite{paper:ntm} described:

\begin{enumerate}
    \item the encoder generates an attention-based representation of the sequence. It consists of a stack of $6$ identical layers. Each layer consists of a multi-head self-attention layer and a classic fully connected feed-forward layer. In between layers a residual connection and normalization is being employed.
    \item the decoder can retrieve a specific piece of information from the encoded representation. It mirrors the encoder architecture.
    \item a final layer connects the outputs of the decoder to the training vocabulary thus yielding a probability distribution over the vocabulary
    \item the training output is thus a one-hot encoded matrix
\end{enumerate}

During training the tokens probability distributions are compared and the attention matrices weights are changed accordingly.\\

The results are superior to what was known at the time and especially effective in the translation task, while using \textit{a small fraction of the training costs of the best models from the literature}.

\subsection{GPT}
\label{subsec:igpt_task-architecture_gpt}

OpenAI's idea is to leverage the (unlabeled) data available and

\begin{enumerate}
    \item deploy a massive layer-based Transformer-only model to learn (unsupervisedly) the representation of the training dataset; this is what they denote \textit{pre-training}
    \item \textit{fine-tune} the representations learned in the specific task/dataset
\end{enumerate}

thus building very asbtract models that can be then trained to specialize on a particular task.

\subsubsection{GPT-1: Improving Language Understanding by Generative Pre-Training}
\label{subsubsec:igpt_task-architecture_gpt1}

GPT-1 is a multi-layer transformer decoder only (without the encoder) where multuple transformer blocks go over the embeddings of input sequence. During fine-tuning dropout is being deployed to generalize better.

\subsubsection{GPT-2: Language Models are Unsupervised Multitask Learners}
\label{subsubsec:igpt_task-architecture_gpt2}

GPT-2 is similar to its predecessor and comes with

\begin{enumerate}
    \item an expansion in vocabulary size
    \item increase context size ($1024$, not $512$)
    \item training over a larger batchsize
\end{enumerate}

\subsubsection{GPT-3: Language Models are Few-Shot Learners}
\label{subsubsec:igpt_task-architecture_gpt3}

GPT-3's architecture is similar to GPT-2's but

\begin{enumerate}
    \item uses 96 layers
    \item modifies the Transformer architecture to match \cite{paper:sparse_trans}
    \item overall has $\times 10$ parameters of GPT-2
\end{enumerate}

\subsection{BERT}
\label{subsec:igpt_task-architecture_bert}

\cite{paper:BERT}, while inheriting from GPT model, has a huge difference: the input is scanned at the same time (there is no direction). This leads to have a completely different loss function and pre-training task:

\begin{enumerate}
    \item \href{https://en.wikipedia.org/wiki/Cloze_test}{cloze test} on the input tokens (usually $P(\text{masked}) \sim 0.15$)
    \item predict next sentence
\end{enumerate}

\subsection{Image GPT}
\label{subsec:igpt_task-architecture_image_gpt}

\cite{paper:igpt} applies GPT model to the input image by flattening the image to a 1D vector of size the number of pixels. Like GPT has a pre-training and a fine-tuning phase and the pre-training stage can be completed using 2 different loss functions:

\begin{enumerate}
    \item autoregressive $\mathcal{L}_{AR}$, makes the NN predict what's the next pixel value given all previous, $p_{i + 1} \mid p_{i}, p_{i - 1}, \ldots$ on a given permutation (usually it's the raster order)
    \item $\mathcal{L}_{BERT}$ is just like \cite{paper:BERT}'s: it masks a percentage (usually $15 \%$) of the input pixels and tries to recover the values of the masked ones from the unmasked
\end{enumerate}

The fine-tuning stage iGPT is either

\begin{enumerate}
    \item a linear probe that just updates a classification head via usual $\mathcal{L}_{CCE}$
    \item or a full fine-tuning of \textit{the entire model on the downstream dataset}
\end{enumerate}

\subsubsection{Applications}
\label{subsubsec:image_gpt-applications}

One of the most straightforward application of a trained iGPT model is the sampling capabilities: it's possible to generate completely new images (out of the training set distribution) starting from scratch. To do so, a distribution on the first pixel has to be known (but that can be easily approximated by taking the mean of the first pixels of some training images) and applying autoregressively the model. In a sense, given an image, iGPT can \textit{complete} it with fair accuracy.\\

Another straightforward application is merging it with a text model \href{https://openai.com/blog/dall-e/}{to generate images from text}.

\subsection{Chosen architecture}
\label{subsec:igpt_task-architecture_chosen}

The final architecture builds on top of iGPT using $\mathcal{L}_{AR}$.

\begin{enumerate}
    \item a pre-processing layer is eventually applied to quantize the image (via K-means clustering). This has been carried out, even if image is grayscale, just to mirror the approach in the original paper.
    \item pre-training is done using $\mathcal{L}_{AR}$
    \item fine-tuning does not really mirror iGPT's respective stage: the aim is to see if a U-Net, trained on the concatenation of the input image and the representation learned (in the pre-training stage), can segment the image with very low amount of (labeled) training data
\end{enumerate}

\section{Loss function}
\label{sec:igpt_task_loss}

Same as \ref{subsec:igpt_task-architecture_image_gpt}'s one. Experiments eventually show that $\mathcal{L}_{AR}$ is slightly more robust (see \ref{sec:igpt_first}).

\section{Implementation}
\label{sec:igpt_task_impl}

The \href{https://github.com/karpathy/minGPT}{karpathy/minGPT} implementation of \cite{paper:igpt} has been forked and taken as basis for the implementation. Code is open-source and available in \href{https://github.com/sirfoga/minGPT}{GitHub}.

\section{Experiments}

\tab[exp-iGPT-config]{} shows the setup for all \textit{experiment runs}: note that it's a very small GPT model (even smaller than GPT-S).\\

A final \textit{sampling} phase consists of running the model (autoregressively or according to the BERT mask) a certain number of steps (at least $\text{number of image pixels} - 1$) to generate a new image.

\subsection{Loss function}
\label{sec:igpt_first}

The aim of these experiments were to see if there was any difference in using $\mathcal{L}_{AR}$ or $\mathcal{L}_{BERT}$. It's difficult to set a objective metrics to evaluate the goodness of the results but, visually \fg[AR-BERT-results]{} shows such difference; \fg[AR-BERT-history]{} shows there is also a difference in training history. This result is in accordance with the findings from \cite{paper:igpt}: \textit{because inputs to the BERT model are masked at training time, we must also mask them at evaluation time to keep inputs in-distribution. This masking corruption may hinder the BERT model's ability to correctly predict image
classes.}

\begin{figure*}[htp]
    \centering
    \showSubFig{60mm}{\figDir/more/toy.png}{Cell nucleus dataset clustered} \showSubFig{60mm}{\figDir/more/brain.png}{EM flybrain dataset clustered} \\
    
    \centering
    \showSubFig{60mm}{\figDir/more/toy/ar.png}{Cell nucleus samples trained on $\mathcal{L}_{AR}$} \showSubFig{60mm}{\figDir/more/cremi/ar.png}{EM flybrain samples trained on $\mathcal{L}_{AR}$} \\

    \centering
    \showSubFig{60mm}{\figDir/more/toy/bert.png}{Cell nucleus samples trained on $\mathcal{L}_{BERT}$} \showSubFig{60mm}{\figDir/more/cremi/bert.png}{EM flybrain samples trained on $\mathcal{L}_{BERT}$} \\

    \caption{Usage $\mathcal{L}_{AR}$ VS $\mathcal{L}_{BERT}$ in training iGPT}
    \label{fig:AR-BERT-results}
\end{figure*}

\subsection{Emebedding method}
\label{sec:igpt_dense}

Compared to the last experiments, clustering has been removed ($256$ colors are now used as \textit{clusters}), and now the aim is to see how the number of dimensions in the embedding and type of embedding influences the output of the NN.

In place of the usual \href{https://pytorch.org/docs/stable/generated/torch.nn.Embedding.html}{embedding layer} a \href{https://pytorch.org/docs/stable/generated/torch.nn.Linear.html#torch.nn.Linear}{linear (i.e dense)} layer has been applied to the one-hot encoding of the input tensor: this executes just a matrix multiplication, so in a sense, is theoretically equivalent to an embedding. However in practice, one can note some differences: \fg[embd-ll-toy-results]{} and \fg[embd-ll-cremi-results]{} show that

\begin{enumerate}
    \item as expected, a high dimensional embedding is better than a low dimensional embedding (both with the usual layer, and with a linear layer)
    \item the usual embedding layer is superior (visually) to the linear layer
\end{enumerate}

\begin{figure*}[htp]
    \centering
    \showSubFig{60mm}{\figDir/more_embd/toy/embd_8/samples.png}{\texttt{nn.Embedding(256, 8)}} \showSubFig{60mm}{\figDir/more_embd/toy/ll_8/samples.png}{\texttt{nn.Linear(64, 8)}} \\

    \centering
    \showSubFig{60mm}{\figDir/more_embd/toy/embd_32/samples.png}{\texttt{nn.Embedding(256, 32)}} \showSubFig{60mm}{\figDir/more_embd/toy/ll_32/samples.png}{\texttt{nn.Linear(64, 32)}} \\

    \centering
    \showSubFig{60mm}{\figDir/more_embd/toy/embd_256/samples.png}{\texttt{nn.Embedding(256, 256)}} \showSubFig{60mm}{\figDir/more_embd/toy/ll_256/samples.png}{\texttt{nn.Linear(64, 256)}} \\

    \caption{Usage of \texttt{nn.Embedding} VS \texttt{nn.Linear} when training iGPT on cell nucleus dataset}
    \label{fig:embd-ll-toy-results}
\end{figure*}

\begin{figure*}[htp]
    \centering
    \showSubFig{60mm}{\figDir/more_embd/cremi/embd_8/samples.png}{\texttt{nn.Embedding(256, 8)}} \showSubFig{60mm}{\figDir/more_embd/cremi/ll_8/samples.png}{\texttt{nn.Linear(64, 8)}} \\
    
    \centering
    \showSubFig{60mm}{\figDir/more_embd/cremi/embd_32/samples.png}{\texttt{nn.Embedding(256, 32)}} \showSubFig{60mm}{\figDir/more_embd/cremi/ll_32/samples.png}{\texttt{nn.Linear(64, 32)}} \\
    
    \centering
    \showSubFig{60mm}{\figDir/more_embd/cremi/embd_256/samples.png}{\texttt{nn.Embedding(256, 256)}} \showSubFig{60mm}{\figDir/more_embd/cremi/ll_256/samples.png}{\texttt{nn.Linear(64, 256)}} \\
    
    \caption{Usage of \texttt{nn.Embedding} VS \texttt{nn.Linear} when training iGPT on EM flybrain}
    \label{fig:embd-ll-cremi-results}
\end{figure*}

Since training on $\mathcal{L}_{AR}$ achieves better results, $\mathcal{L}_{AR}$ is the only loss considered in future experiments.

\subsection{Replacing embeddings}
\label{sec:igpt_Manual_embeddings}

The aim of these \textit{experiment runs} is to see if it's possible to replace the embedding construction with something simpler (and faster).

\subsubsection{Double convolution}
\label{subsec:igpt_double_conv}

The approach taken is to

\begin{enumerate}
    \item normalize the input pixels ($\in [0, 1]$)
    \item convoluting (\href{https://pytorch.org/docs/stable/generated/torch.nn.Conv1d.html}{1D}) the input with a kernel to transform the input from \texttt{batch size} $\times$ \texttt{number of context tokens} $\rightarrow$ \texttt{batch size} $\times$ \texttt{number of context tokens} $\times$ \texttt{intermediate dimensions}; passing the result to a non-linear activation (like \href{https://pytorch.org/docs/stable/generated/torch.nn.LeakyReLU.html}{nn.LeakyReLU})
    \item convoluting (1D) a second time the output from last step to get a tensor \texttt{batch size} $\times$ \texttt{number of context tokens} $\times$ \texttt{embedding dimensions}
\end{enumerate}

This results in a too simplistic view of the image and sampling a trained model yields random noise pixels, as seen in \fg[art_emdb_0]{}.

\begin{figure*}[htp]
    \centering
    \showSubFig{120mm}{\figDir/diff_embd/blow_samples_toy.png}{Samples of iGPT trained on cell nucleus dataset using 2-times 1D convolutions as embedding} \\
    
    \centering
    \showSubFig{120mm}{\figDir/diff_embd/blow_samples_em.png}{Samples of iGPT trained on EM flybrain dataset using 2-times 1D convolutions as embedding}
        
    \caption{Results of trraining iGPT via artificial embeddings (2-times 1D convolutions)}
    \label{fig:art_emdb_0}
\end{figure*}

\subsubsection{Artificial embedding}

The immediate reaction is to try to let the NN start from a \textit{good guess} for the first convolution; the pipeline then becomes:

\begin{enumerate}
    \item artificially construct a $n$-dimensional vector for each pixel that represents its embedding: this vector theoretically should contain information regarding the pixel value and should differ based on pixel value.
    \item second 1D convolution 
\end{enumerate}

This set of experiments uses a Gaussian ($\sim \mathcal{N}(\mu = \text{pixel value}, \sigma=1)$) as an artificial embedding: \fg[art_embd_gauss]{} shows the embedding for some random pixel values $\in [0, 1]$: the $x$-axis is a $d=32$-dimensional intermediate embedding, while the $y$-axis is the value of the $x$-th component of the embedding. Two normalization strategies can be applied:

\begin{enumerate}
    \item softmax-ing the result of the Gaussian, thus yielding a normalized probability distribution function
    \item maxing-out by dividing the result of the Gaussian by its largest value
\end{enumerate}

Both the two strategies yield random results (similar to \ref{subsec:igpt_double_conv}).
