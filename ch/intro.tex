\chapter{Introduction}
\label{cap:intro}

\section{Outline}
\label{sec:intro-outline}

\begin{enumerate}[i]
    \item this chapter describes the problem that I am trying to solve and the approach I'm using
    \item chapter \ref{cap:data} describes the data used
    \item chapter \ref{cap:toy_task} describes what has been done with respect to the segmentation task of cell nucleus images: a comparative study of the hyperparameters that define an abstract U-Net architecture
    \item chapter \ref{cap:igpt} analyzes the usage of Transformer-based architectures in the unsupervised segmentation problem
    \item chapter \ref{cap:discuss} finalizes the report and presents the findings of the project
\end{enumerate}

\section{Problem}
\label{sec:intro-Problem}

Image segmentation is a long-standing problem in the computer vision field, where

\begin{enumerate}
    \item the input is a digital (multi-channel) image
    \item the output is a semantic partition of the image into connected regions
\end{enumerate}

Formally each pixel has to be classified into one (possibly many) labels.

\subsection{Motivation}
\label{subsec:intro-Motivation}

Solving the segmentation problem is extremely useful (needed) in many computer vision applications (self-driving cars, robotics, automated surgery ...); but the scientific area which benefit the most from novel computer vision techniques is microscopy image analysis.\\

Recent studies (\cite{paper:WeigertCARE}) have confirmed that, especially machine learning, can contribute immensely to the quality of microscopy images.\\

Thus solving the segmentation of microscopy images is both useful in practice and can contribute novel approaches in the computer vision field.

\subsection{Objective}
\label{subsec:intro-objective}

The aim of the project is to investigate the usefulness of different classic segmentation methods for the special task of cell segmentation in microscopy data. \\

Moreover recent trends (\cite{paper:att_need}) in \textit{attention} mechanisms have proven to be effective and applicable in real world scenarios. The project investigates whether \textit{attention} is really a valuable tool in analyzing microscopy images.

\section{History}
\label{sec:intro-history}

\subsection{Classic approaches}
\label{sec:intro-classic}

Since the invention of the digital camera, the segmentation has attracted a lot of computer vision field's attention and many methods now exist to deal with it:

\begin{enumerate}
    \item \cite{paper:SLIC} suggests the use of \textit{superpixels} that can be (eventually) joined to create a higher level segmentation. The problem it suffers from is that it's difficult to have meaningful segmentation in images where the semantic is hidden (\ie{} not apparent pixel-wise)
    \item segmentation via \href{https://scikit-learn.org/stable/modules/clustering.html\#overview-of-clustering-methods}{clustering} (K-means, EM, mean shift) is also a popular way to find partitions of the image based on the division into clusters in a (feature) space. These methods are not fully automatic and need human intervention.
    \item \cite{paper:seam_carving} solves a somewhat similar problem (image retargeting) by \textit{carving out appropriately-chosen seam segments} altough it's not completely reliable on some images
\end{enumerate}

\subsection{Artificial neural networks}
\label{sec:intro-nn}

In the past years CNNs have become the standard method to solve segmentation tasks in computer vision in general and microscopy image analysis. This \href{https://www.youtube.com/watch?v=aisgNLypUKs}{can be considered} the 2nd/3rd generation of artificial NNs (\href{https://en.wikipedia.org/wiki/Multilayer_perceptron}{MLP}, non-linear activations, \href{https://en.wikipedia.org/wiki/Spiking_neural_network}{SNN}), trending now due to recent hardware and data development.\\

One thing that features this generation of NNs is the \href{https://www.youtube.com/watch?v=8TTK-Dd0H9U}{development of unsupervised techniques} due to

\begin{enumerate}
    \item insufficient number of people able to label highly-specific datasets
    \item astonishingly huge amount of data available
\end{enumerate}

\subsection{State of the art}
\label{sec:intro-sota}

It's difficult to grab the SOTA for a given problem due to

\begin{enumerate}
    \item amount of SOTAs per month
    \item different problem conditions
    \item unequal quality of training datasets (even proprietary datasets can be used to claim SOTA)
    \item unequal training power (\ie{} money) among participants
\end{enumerate}

While \cite{paper:eval_deep_nucl_segm} surveys the latest advancements in deep learning segmentation, it can be said that \cite{paper:unet} marked the start of the all-in deep-learning methods in microscopy data. Their approach, while being improved many times in the future, has been a milestone in image segmentation. \\

That being said, the U-Net is considered the SOTA architecture of the problem.

\section{Attention}
\label{sec:intro-attention}

Recently, the incorporation of attention mechanisms \seec[paper:self-calibr-conv] into existing NN architectures have proven to increase overall performance on many tasks \seec[paper:attention_pancreas].\\

The term \textit{attention} is meant to describe the process by which the NN learns where to focus in order to perceive and classify at best the task. This is a very abstract concept and can be translated into many concrete examples:

\begin{enumerate}
    \item \textit{self-attention} specifies how the input re-routes to the output
    \item \textit{multi-head attention} computes a linear transformation of independent attention outputs: \textit{allows the model to jointly attend to information from different representation subspaces at different positions. With a single attention head, averaging inhibits this.}
\end{enumerate}

\section{Implementation}
\label{sec:intro-impl}

Code for the project is open-source and available on GitHub/GitLab:

\begin{enumerate}
    \item \href{https://github.com/sirfoga/attila}{sirfoga/attila} is the experimental version of the dense image segmentation task
    \item \href{https://github.com/sirfoga/minGPT}{sirfoga/minGPT} is the fork of \href{https://github.com/karpathy/minGPT}{Karpathy's minGPT}
    \item \href{https://gitlab.com/thefoga/tud/rpr}{tud/rpr} contains the sources to build this document
\end{enumerate}
