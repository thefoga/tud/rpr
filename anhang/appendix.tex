\include{./data/toy_task}

\let\cleardoublepage\clearpage
\chapter{Dense image segmentation task}
\label{app-toy_task-model-input}

\section{Considerations on U-Net input size}

Assume, without loss of generality, a square image as input (\ie{} height = width) and let

\begin{enumerate}
    \item $d \in \mathbb{N}_0$ the number of (contracting/expanding) layers
    \item $\gamma \in \mathbb{N}_0$ the cropped size in a single layer due to convolutions
    \item $\alpha \in \mathbb{N}_0$ the size of the input at the bottomest layer
    \item $\beta$ the height (and also the width)
\end{enumerate}
we enforce
\begin{equation}
\beta = 2 ^ d \cdot \alpha + (2 ^ d - 1) \cdot \gamma \label{eq:calc_unet_out_shape.1}
\end{equation}
so that
\begin{equation}
\alpha = \frac{\beta - (2 ^ d - 1) \cdot \gamma}{2 ^ d} \in \mathbb{N}_0 \label{eq:calc_unet_out_shape.2}
\end{equation}

\begin{exmp}
  Given $4$ layers and $2$ convolutions (kernel size = $3$) per block, an input size of $508 \times 508$ pixels would result in $\alpha = \frac{508 - (2 ^ 4 - 1) \cdot (3 - 1) ^ 2}{2 ^ 4} = 28 \in \mathbb{N}$ and thus is suitable.
\end{exmp}

\begin{exmp}
Given $3$ layers and $2$ convolutions (kernel size = $3$) per block, an input size of \inpsize{} $\times$ \inpsize{} pixels would force $\alpha{} \not \in \mathbb{N}_0$ and thus is not suitable. It is assumed that the input size is checked by the user of the NN
\end{exmp}

This means that, given same input size, there may be some cropping (let $\delta$ the necessary cropping) based on the padding.
In particular given $\alpha_{\text{padding}}, \beta_{\text{padding}}$ the size of the input at the bottomest layer and the height (and the width) of the input image fed to a network using padding = $\text{padding}$ respectively we need

\begin{align}
\beta_{\text{valid}} &= \beta_{\text{same}} + \delta \iff \\
2 ^ d \cdot  \alpha_{\text{valid}} + (2 ^ d - 1) \cdot  \gamma &= 2 ^ d \cdot  \alpha_{\text{same}} + \delta \iff \\
\alpha_{\text{valid}} &= \frac{2 ^ d \cdot  \alpha_{\text{same}} + \delta - (2 ^ d - 1) \cdot  \gamma}{2 ^ d}  \iff \\
\alpha_{\text{valid}} &= \alpha_{\text{same}} + \frac{\delta - 2 ^ d \cdot \gamma - \gamma}{2 ^ d}  \iff \\
\alpha_{\text{valid}} &= \alpha_{\text{same}} - \gamma + \frac{\delta - \gamma}{2 ^ d}
\end{align}

and since $\alpha_{\text{valid}} \in \mathbb{N}_0$ we need

\begin{equation}
\delta - \gamma \equiv 0 \mod 2 ^ d
\end{equation}

Obviously one choice would be to $\delta := \gamma$, and that's what has been done.

\begin{exmp}
  Given $4$ layers and $2$ convolutions (kernel size = $3$) per block, $\gamma = 4$ and thus an input size \inpsize{} $\times$ \inpsize{} (suitable for padding = \texttt{same}) has to endure a cropping ($\delta = \gamma = 4$) to a size of 508 $\times$ 508 before being fed to the network using padding = \texttt{valid}.
\end{exmp}

\section{Training histories}

\showFig{160mm}{\figDir/trials/optimizers/lr.png}{Effect of different learning rates when training a U-Net}{small_experiments_lrs}

\begin{figure*}[htp]

\centering
\showSubFig{70mm}{\figDir/trials/optimizers/adam, lr = 1e-2/\histToShow}{adam, lr = 1e-2} \showSubFig{70mm}{\figDir/trials/optimizers/adam, lr = 1e-3/\histToShow}{adam, lr = 1e-3} \\

\centering
\showSubFig{70mm}{\figDir/trials/optimizers/adam, lr = 1e-4/\histToShow}{adam, lr = 1e-4} \showSubFig{70mm}{\figDir/trials/optimizers/adam, lr = 1e-5/\histToShow}{adam, lr = 1e-5} \\

\centering
\showSubFig{70mm}{\figDir/trials/optimizers/sgd, high momentum/\histToShow}{sgd, lr = 1e-2, momentum = 0.99}

\caption{Effect of choosing different optimizers when training a U-Net. \histLegend}
\label{fig:small_experiments_optimizers}
\end{figure*}

\showFig{160mm}{\figDir/trials/to-aug-or-not/no-aug.png}{Effect of training/validation splits on non-augmented data when training a U-Net}{small_experiments_no_aug}

\showFig{160mm}{\figDir/trials/to-aug-or-not/aug.png}{Effect of training/validation splits on augmented data when training a U-Net}{small_experiments_aug}

\begin{figure*}[htp]

\centering
\showSubFig{70mm}{\figDir/trials/to-aug-or-not/no-aug/\histToShow}{no augmentation} \showSubFig{70mm}{\figDir/trials/to-aug-or-not/aug/\histToShow}{data augmented} \\

\caption{Effect of data augmentation when training a U-Net. \histLegend}
\label{fig:small_experiments_augs}
\end{figure*}

\begin{figure*}[htp]

\centering
\showHistoryOfModel{with_same}{no-aug} \showHistoryOfModel{without_same}{no-aug} \\

\centering
\showHistoryOfModel{with_valid}{no-aug} \showHistoryOfModel{without_valid}{no-aug} \\

\centering
\showHistoryOfModel{with_same_se}{no-aug} \showHistoryOfModel{without_same_se}{no-aug} \\

\centering
\showHistoryOfModel{with_valid_se}{no-aug} \showHistoryOfModel{without_valid_se}{no-aug} \\

\caption{Last \lastepochs{} epochs of training a U-Net. \histLegend}
\label{fig:usenet-history}
\end{figure*}

\begin{figure*}[htp]

\centering
\showHistoryOfModel{with_same}{aug} \showHistoryOfModel{with_valid}{aug} \\
\showHistoryOfModel{with_same_se}{aug} \\

\caption{Last \lastepochs{} epochs of training of training a U-Net on augmented data. \histLegend}
\label{fig:usenet-history-augm}
\end{figure*}

\include{./data/iGPT}

\chapter{Generative Image Models with Transformers}

\begin{figure*}[htp]
    \centering
    \showSubFig{60mm}{\figDir/first/ar.png}{$\mathcal{L}_{AR}$ loss} \showSubFig{60mm}{\figDir/first/bert.png}{$\mathcal{L}_{BERT}$ loss} \\

    \caption{$\mathcal{L}_{AR}$ VS $\mathcal{L}_{BERT}$ iGPT training history (50 epochs)}
    \label{fig:AR-BERT-history}
\end{figure*}

\showFig{160mm}{\figDir/diff_embd/art_embd_gauss.png}{Artificially constructed embeddings from a Gaussian PDF}{art_embd_gauss}
