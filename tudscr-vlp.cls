\newcommand{\partsFolder}{parts}

\NeedsTeXFormat{LaTeX2e}[2020/02/02]
\ProvidesClass{tudscr-vlp}[2020/04/02 v1.00 style template for TUD-Script (VLP)]
\RequirePackage{tudscrbase}
\DefineFamily{VLP}
\DefineFamilyMember{VLP}
\newcommand*\vlp@baseclass{}
\let\vlp@baseclass\relax
\AtEndOfClass{\RelaxFamilyKey{VLP}{baseclass}}
\DefineFamilyKey{VLP}{baseclass}{%
  \FamilySetNumerical{VLP}{baseclass}{@tempa}{%
    {book}{0},{scrbook}{0},{tudscrbook}{0},%
    {report}{1},{scrreprt}{1},{scrreport}{1},{tudscrreprt}{1},{tudscrreport}{1}%
  }{#1}%
  \ifx\FamilyKeyState\FamilyKeyStateProcessed%
    \ifcase\@tempa\relax%
      \def\vlp@baseclass{tudscrbook}%
    \else%
     \def\vlp@baseclass{tudscrreprt}%
   \fi%
  \fi%
}
\AtEndOfClass{\RelaxFamilyKey{VLP}{pdfminorversion}}
\DefineFamilyKey{VLP}{pdfminorversion}{%
  \FamilyKeyStateUnknownValue%
  \Ifisinteger{#1}{%
    \ifnum\numexpr#1\relax>\z@%
      \iftutex%
        \ifluatex%
          \directlua{pdf.setminorversion(#1)}%
          \FamilyKeyStateProcessed%
        \fi%
        \ifxetex%
          \special{pdf:minorversion #1}%
          \FamilyKeyStateProcessed%
        \fi%
      \else%
        \ifpdftex%
          \pdfminorversion=#1%
          \FamilyKeyStateProcessed%
        \fi%
      \fi%
    \fi%
  }{}%
}

\newcount\vlp@type
\vlp@type=\m@ne
\newif\if@vlp@student
\AtEndOfClass{\RelaxFamilyKey{VLP}{vlptype}}
\DefineFamilyKey{VLP}{vlptype}{%
  \FamilySetNumerical{VLP}{baseclass}{vlp@type}{%
    {habil}{0},{diss}{1},{diplom}{2},%
    {sta}{3},{beleg}{4},{other}{5}%
  }{#1}%
  \@vlp@studentfalse%
  \ifx\FamilyKeyState\FamilyKeyStateProcessed%
    \ifcase\vlp@type\relax% habil
    \or% diss
    \or% diplom
      \@vlp@studenttrue%
    \or% sta
      \@vlp@studenttrue%
    \or% beleg
      \@vlp@studenttrue%
    \or% other
    \fi%
  \fi%
}
\newcommand*\vlp@pass@options{}
\DeclareOption*{\eappto\vlp@pass@options{,\expandonce\CurrentOption}}%
\FamilyProcessOptions{VLP}\relax
\ifx\vlp@baseclass\relax%
  \ifnum\vlp@type>\tw@\relax%
    \FamilyOptions{VLP}{baseclass=report}%
  \else%
    \FamilyOptions{VLP}{baseclass=book}%
  \fi%
\fi%
\PassOptionsToPackage{headinclude=true,footinclude=false}{typearea}
%\if@vlp@student%
%  \PassOptionsToClass{titlesignature}{\vlp@baseclass}%
%\fi%
\PassOptionsToClass{%
  cdfont=false,
  cd=false,
  cdtitle=true,
  cdgeometry=false,
  \if@vlp@student%
    titlesignature,
  \fi%
% TODO bugfix paper=a4
  fontsize=11pt,
  DIV=12,
  BCOR=5mm,
  twoside,
  open=right,
  numbers=noenddot,
  captions=tableheading,
  % optionales Argument der Gliederungsbefehle erweitert (s. scrguide.pdf)
  headings=optiontoheadandtoc,
  \vlp@pass@options
}{\vlp@baseclass}
\LoadClass{\vlp@baseclass}
\newcommand*\vlp@keywords{}
\newcommand*\vlp@english@keywords{}
\newcommand*\vlp@german@keywords{}
\newcaptionname{ngerman,german}{\vlpkeywords}{\vlp@german@keywords}
\newcaptionname{english}{\vlpkeywords}{\vlp@english@keywords}
\newcommand*\addkeywords[2][]{%
  \begingroup%
    \def\@tempa##1##2{%
      \def\@tempb{##2}%
      \trim@spaces@in\@tempb%
      \expandafter\ifinlist\expandafter{\@tempb}{##1}{}{%
        \listxadd##1{\expandonce\@tempb}%
      }%
    }%
    \forcsvlist{\@tempa{\vlp@german@keywords}}{#1}%
    \forcsvlist{\@tempa{\vlp@english@keywords}}{#2}%
  \endgroup%
}
\AfterClass!{\vlp@baseclass}{%
  \addtokomafont{disposition}{\cdfontsn}
  \faculty{%
    Fakult\"at %
    \quotedblbase Informatik\textquotedblleft%
  }
  \institute{MLCV, Institute of AI}
  \chair{Faculty of Computer Science}
  \professor{Dr.-Ing. Birgit Jaekel}
  \place{Dresden}
  % TODO map to tudscr types and corresponding localization
  \ifcase\vlp@type\relax% habil
    \graduation{Dr.-Ing. habil.}%
    \thesis{Habilitation}%
  \or% diss
    \graduation[Dr.-Ing.]{Doktor-Ingenieur}%
    \thesis{Dissertation}%
  \or% diplom
    \graduation[Dipl.-Ing.]{Diplom-Ingenieur}%
    \thesis{Diplomarbeit}%
  \or% sta
    % \thesis{Studienarbeit}%
    \thesis{Research project report}%
  \or% beleg
    \thesis{Belegarbeit}%
  \or% other
  \fi%
  \input{metadata}%
}

\tud@localization@english{\datetext}{Last updated on}


\PassOptionsToPackage{%
  plainpages=false,
  hypertexnames=false,
  breaklinks=true,
%  pdfpagelabels=true,
%  pdfa=true
}{hyperref}
\newcommand*\vlp@pdf@info{}
\AfterPackage{hyperref}{%
  \renewcommand*\vlp@pdf@info{%
    \begingroup%
      \def\and{, }%
      \let\thanks\@gobble%
      \let\footnote\@gobble%
      \let\vlp@keywords\@empty%
      \def\@tempa##1{%
        \ifx\vlp@keywords\@empty\else%
          \appto\vlp@keywords{,}%
        \fi%
        \appto\vlp@keywords{##1}%
      }%
      \forlistloop\@tempa{\vlp@english@keywords}%
      \forlistloop\@tempa{\vlp@german@keywords}%
      \def\@tempa##1{\cslet{##1}\@gobble}%
      \expandafter\forcsvlist\expandafter\@tempa\expandafter{%
        \tud@split@author@list%
      }%
      \hypersetup{%
        pdfauthor   = {\@author},%
        pdftitle    = {\@title},%
%        pdfsubject  = {\@subtitle},%       \@subject vs. \@@thesis
        pdfkeywords = {\vlp@keywords},%
      }%
    \endgroup%
  }%
}
\ifcsundef{tud@v@2.07}{\@tempswatrue}{\@tempswafalse}
\newif\if@mathsub@upright
\if@tempswa
  % TODO option
  % TODO catcodes in marcos für true/false
  \AtBeginDocument{\catcode`\_=12 \mathcode`\_="8000}
  \newcommand*\mathsub@set[1]{%
    \if@mathsub@upright%
      \sb{\mathrm{#1}}%
    \else%
      \sb{\mathit{#1}}%
    \fi%
  }%
  \newcommand*\mathsub@swap{}
  \def\mathsub@swap|#1|{%
    \begingroup%
      \swapuprightsubscripts%
      \mathsub@set{#1}%
    \endgroup%
  }%
  \begingroup%
    \catcode`\_=13%
    \gdef_{\@ifnextchar|\mathsub@swap\mathsub@set}%
  \endgroup%
  \newcommand*\enableuprightsubscripts{\@mathsub@uprighttrue}%
  \newcommand*\disableuprightsubscripts{\@mathsub@uprightfalse}%
  \newcommand*\swapuprightsubscripts{%
    \if@mathsub@upright%
      \@mathsub@uprightfalse%
    \else%
      \@mathsub@uprighttrue%
    \fi%
  }%
  % TODO patch raus, sobald das in tudscr gefixt ist
  \xapptocmd\tud@maketitle{%
    \begingroup%
      \edef\@tempa{%
        \endgroup%
        \ifcase\tud@cd@title@num\relax\else% !false
          \if@titlepage%
            \unexpanded{\let\tud@currentgeometry\@empty}%
            \noexpand\tud@loadgeometry{\tud@currentgeometry}%
          \fi%
        \fi%
      }%
    \@tempa%
  }{}{\tud@patch@wrn{tud@maketitle}}%
\fi
% TODO new data type for @standard and \DeclareLabelalphaTemplate[standard]{}
% https://tex.stackexchange.com/questions/175776
\AfterPackage{biblatex}{%
  \RequirePackage{url}
  \renewcommand*{\labelalphaothers}{}
  \renewcommand*{\mkbibnamefamily}[1]{\textsc{#1}}
  \renewcommand*{\labelnamepunct}{\addcolon\addspace}
  \DeclareLabelalphaTemplate{%
    \labelelement{%
      \field[final]{shorthand}
      \field{label}
      \field[strwidth=3, names=1]{labelname}
      \field[strwidth=3]{editor}
      \field[strwidth=3]{number}
    }
    \labelelement{%
      \field[strwidth=2,strside=right]{year}
    }
  }
  \DeclareFieldFormat[patent]{note}{\bibstring{anmeldetag} #1}
  \DeclareFieldFormat[patent]{date}{(#1)}
  % Bibliography strings
  \NewBibliographyString{patent}
  \NewBibliographyString{norm}
  \NewBibliographyString{anmeldetag}
  \DefineBibliographyStrings{german}{
    norm       = Norm,
    patent     = Schutzrecht,
    anmeldetag = Pr. (Anmeldetag),
  }
  \DefineBibliographyStrings{ngerman}{
    norm       = Norm,
    patent     = Schutzrecht,
    anmeldetag = Pr. (Anmeldetag),
  }
  \DefineBibliographyStrings{english}{
    norm       = Standard,
    patent     = Patent,
  }
  \DeclareBibliographyDriver{online}{%
    \usebibmacro{bibindex}%
    \usebibmacro{begentry}%
    \ifnameundef{author}
      {\iflistundef{organization}
        {\BibliographyWarning{No author nor organisation on Online entry}}
        {\printlist{organization}}}
      {\usebibmacro{author/editor+others/translator+others}}
    \setunit{\labelnamepunct}\newblock
    \usebibmacro{title}%
    \newunit
    \printlist{language}%
    \newunit\newblock
    \usebibmacro{byauthor}%
    \newunit\newblock
    \usebibmacro{byeditor+others}%
    \newunit\newblock
    \printfield{version}%
    \newunit
    \printfield{note}%
    \newunit\newblock
    \ifnameundef{author}
      {}
      {\printlist{organization}}
    \newunit\newblock
    \usebibmacro{date}%
    \newunit\newblock
    \iftoggle{bbx:eprint}
      {\usebibmacro{eprint}}
      {}%
    \newunit\newblock
    \usebibmacro{url+urldate}%
    \newunit\newblock
    \usebibmacro{addendum+pubstate}%
    \setunit{\bibpagerefpunct}\newblock
    \usebibmacro{pageref}%
    \usebibmacro{finentry}%
  }
  \DeclareBibliographyDriver{patent}{%
    \usebibmacro{bibindex}%
    \usebibmacro{begentry}%
    \bibstring{patent}
    \newblock
    \printfield{number}%
    \newblock\setunit*{\addspace}
    \usebibmacro{date}
    \newunit\newblock
    \printfield{note}%
    \usebibmacro{author}
    \setunit{\labelnamepunct}\newblock
    \usebibmacro{title}%
    \newunit
    \printlist{language}%
    \setunit{\bibpagerefpunct}\newblock
    \usebibmacro{pageref}
    \newunit\newblock
    \usebibmacro{related}
    \usebibmacro{finentry}
  }
  \DeclareBibliographyDriver{misc}{%
    \usebibmacro{bibindex}%
    \usebibmacro{begentry}%
    \bibstring{norm}
    \printfield{type}%
    \newblock\setunit*{\addspace}
    \printfield{number}%
    \newblock\setunit*{\addspace}
    \usebibmacro{date}
    \newunit\newblock
    \printfield{note}%
    \usebibmacro{author}
    \setunit{\labelnamepunct}\newblock
    \usebibmacro{title}%
    \newunit
    \printlist{language}%
    \setunit{\bibpagerefpunct}\newblock
    \usebibmacro{pageref}
    \newunit\newblock
    \usebibmacro{related}
    \usebibmacro{finentry}
  }
}
\AtBeginDocument{%
  % \input{hyphenations}{}{}%
  % schräggestellter bruch
  \providecommand*\nfrac[2]{%
    \leavevmode%
    \kern.1em\raise.5ex\hbox{\scriptsize #1}%
    \kern-.1em/\kern-.15em%
    \lower.25ex\hbox{\scriptsize #2}%
  }%
}
\newcommand*\vlp@temp{}
\newcount\vlp@cnt
\newcommand*\vlp@calc[3]{%
  \vlp@cnt=\numexpr#1\relax%
  \the\vlp@cnt\space#2\ifnum\vlp@cnt=\@ne\else#3\fi%
}
\newcommand*\vlp@footnote{}
\newcommand*\vlp@currentpagestyle{}
\newcommand*\vlpfrontmatter[2][]{%
  \vlp@pdf@info%
  \renewcommand*\addkeywords[2][]{%
    \ClassError{tudscr-vlp}{Command \string\addkeywords\space too late}{%
      The command \string\addkeywords\space can only be used\MessageBreak%
      before \string\vlpfrontmatter.%
    }%
  }%
  \let\vlp@footnote\thefootnote%
  \let\vlp@currentpagestyle\currentpagestyle%
  \if@vlp@student%
    \let\vlp@keywords\@empty%
    \def\vlp@temp##1{%
      \ifx\vlp@keywords\@empty\else%
        \appto\vlp@keywords{,\space}%
      \fi%
      \appto\vlp@keywords{##1}%
    }%
    \expandafter\forlistloop\expandafter\vlp@temp\expandafter{\vlpkeywords}%
    \uppertitleback{%
      \addsec*{Abstract}%
      \tudbookmark[0]{Abstract}{abstract}%
      \input{abstract}%
      \par\bigskip\noindent\vlp@keywords%
    }%
  \fi%
  \ifx\@@thesis\@empty\else%
    \lowertitleback{%
      \iftotalpages
        \ifundef\vlp@main@pages{%
          \def\vlp@temp{%
            \vlp@calc{\totalpages-\if@vlp@student2\else1\fi}{Seite}{n},
            \vlp@calc{\totalfigures}{Abbildung}{en},
            \vlp@calc{\totaltables}{Tabelle}{n}%
            \ifundef\vlp@cites{}{, \vlp@cites\space Quellen}%
          }%
        }{%
          \def\vlp@temp{%
            \vlp@calc{\vlp@main@pages}{Seite}{n},
            \vlp@calc{\vlp@main@figures}{Abbildung}{en},
            \vlp@calc{\vlp@main@tables}{Tabelle}{n}%
            \ifundef\vlp@cites{}{, \vlp@cites\space Quellen}%
            \newline Anhang:
            \vlp@calc{%
              \totalpages-\if@vlp@student2\else1\fi-\vlp@main@pages%
            }{Seite}{n},
            \vlp@calc{\totalfigures-\vlp@main@figures}{Abbildung}{en},
            \vlp@calc{\totaltables-\vlp@main@tables}{Tabelle}{n}%
          }%
        }%
      \else%
        \def\vlp@temp{}%
      \fi%
    }%
  \fi%
  \maketitle[{cdgeometry=true,#1}]%
  \renewcommand*\thefootnote{\footnotesize\textrm\Roman{footnote}}%
  \pagestyle{plain}%
  \csname frontmatter\endcsname%
  \setcounter{page}{1}%
  \pagenumbering{Roman}%
  % \input{vorspann}{}{}%
  \if@vlp@student%
    % todo \addchap*{Thesis}%
    % \tudbookmark[0]{Thesis}{thesis}%
    % \input{thesen}%
  \fi%
  \tableofcontents%
  \cleardoublepage%
  #2%
  \let\thefootnote\vlp@footnote%
  \setcounter{footnote}{0}%
  \cleardoublepage%
  \csname mainmatter\endcsname%
  \setcounter{page}{1}%
  \pagenumbering{arabic}%
  \expandafter\pagestyle\expandafter{\vlp@currentpagestyle}%
  \renewcommand*\vlpfrontmatter[2][]{%
    \ClassWarning{tudscr-vlp}{\string\vlpfrontmatter\space was already used}%
  }%
}
\newcommand*\vlp@backmatter[1]{%
  \ClassWarning{tudscr-vlp}{\string\vlpbackmatter\space was already used}%
}
\newcommand*\vlpbackmatter[1]{%
  \csname backmatter\endcsname
  #1%
  % todo \if@vlp@student\confirmation\fi%
  \let\vlpbackmatter\vlp@backmatter%
}
\AtEndDocument{%
  \ifx\vlpbackmatter\vlp@backmatter\else%
    \vlpbackmatter{}%
  \fi%
}
\RequirePackage{totalcount}
\DeclareTotalCounter{figure}
\DeclareTotalCounter{table}
\DeclareTotalCounter{page}
\newcommand*\vlp@appendix{%
  \clearpage%
  \if@filesw%
    \immediate\write\@mainaux{%
      \string\gdef\string\vlp@main@pages{\the\numexpr\value{page}-1\relax}^^J%
      \string\gdef\string\vlp@main@figures{\the\value{totalcount@figure}}^^J%
      \string\gdef\string\vlp@main@tables{\the\value{totalcount@table}}%
    }%
  \fi%
  \cleardoubleemptypage%
  \begingroup%
    \renewcommand*\partpagestyle{empty}%
    \addpart{\appendixname}%
  \endgroup%
  \renewcommand*\ext@toc{atoc}%
  % todo \listoftoc[Verzeichnis der Anhänge]{atoc}%
  \cleardoubleemptypage%
}
\ifundef\appendixmore{\def\appendixmore{}}{}
\appto\appendixmore{\vlp@appendix}
\AfterPackage{biblatex}{%
  \newcounter{vlp@cites}
  \AtDataInput{\stepcounter{vlp@cites}}
  \AtEndDocument{%
    \if@filesw%
      \immediate\write\@mainaux{%
        \string\gdef\string\vlp@cites{\the\value{vlp@cites}}%
      }%
    \fi%
  }%
}
\endinput
